//
//  addNewViewController.h
//  finance
//
//  Created by Mark Farrell on 2/12/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface addNewViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate>
{
    UIPickerView* picker;
    NSArray* arrayOfTitles;
    UITextField* title;
    UITextField* amount;
    UIDatePicker* datePicker;
    NSString* dateString;
    
    BOOL isExpense;
}

@property (nonatomic, strong)NSMutableDictionary* nItem;

@end

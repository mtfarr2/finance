//
//  addNewViewController.m
//  finance
//
//  Created by Mark Farrell on 2/12/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "addNewViewController.h"

@interface addNewViewController ()

@end

@implementation addNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isExpense = YES;
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(SaveBtnTouched:)];
    
    NSArray *actionButtonItems = @[saveBtn];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    arrayOfTitles = @[@"Expense", @"Income"];
    
    picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 216)];
    picker.dataSource = self;
    picker.delegate = self;
    [self.view addSubview:picker];
    
    title = [[UITextField alloc]initWithFrame:CGRectMake(40, picker.frame.origin.y + picker.frame.size.height, self.view.frame.size.width - 80, 50)];
    title.placeholder = [NSString stringWithFormat:@" Title"];
    [title.layer setBorderWidth:1];
    [title.layer setCornerRadius:5];
    [self.view addSubview:title];
    
    amount = [[UITextField alloc]initWithFrame:CGRectMake(40, title.frame.origin.y + title.frame.size.height + 20, self.view.frame.size.width - 80, 50)];
    amount.placeholder = [NSString stringWithFormat:@" Amount"];
    [amount.layer setBorderWidth:1];
    [amount.layer setCornerRadius:5];
    [self.view addSubview:amount];
    
    datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, amount.frame.origin.y + amount.frame.size.height + 20, self.view.frame.size.width, 216)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    dateString = [formatter stringFromDate:datePicker.date];
    [self.view addSubview:datePicker];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)SaveBtnTouched:(id)sender
{
    
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    CGFloat amountNum = [amount.text floatValue];
    
    self.nItem = [NSMutableDictionary new];
    [self.nItem setObject:title.text forKey:@"Title"];
    [self.nItem setObject:amount.text forKey:@"Amount"];
    [self.nItem setObject:dateString forKey:@"Date"];
  
    
    if (isExpense) {
        [ad.expenseData addObject:self.nItem];
        ad.bal -= amountNum;
    }else{
        [ad.incomeData addObject:self.nItem];
        ad.bal += amountNum;
    }
    [self.navigationController popViewControllerAnimated:YES];


}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return arrayOfTitles[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([arrayOfTitles[row]  isEqual: @"Expense"]) {
        isExpense = YES;
    } else {
        isExpense = NO;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

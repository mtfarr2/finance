//
//  ViewController.m
//  finance
//
//  Created by Mark Farrell on 2/4/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    RootViewController* rvc = [RootViewController new];
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:rvc];
    [self addChildViewController:nav];
    [self.view addSubview:nav.view];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

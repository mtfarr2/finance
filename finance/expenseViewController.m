//
//  expenseViewController.m
//  finance
//
//  Created by Mark Farrell on 2/4/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "expenseViewController.h"


@interface expenseViewController ()

@end

@implementation expenseViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self){
        self.title = @"Expense";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    CGRect barFrame = self.navigationController.navigationBar.frame;
    CGRect tabFrame = self.tabBarController.tabBar.frame;
    self->myTableView.contentInset = UIEdgeInsetsMake((barFrame.origin.y + barFrame.size.height), 0, tabFrame.size.height, 0);
    
    [self.view addSubview:myTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
}

-(void)loadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    array = ad.expenseData;
    
    [myTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return array.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString* balanceString = [NSString stringWithFormat:@"%.02f", ad.bal];
    NSString* sectionName = @"Balance: $";
    sectionName = [sectionName stringByAppendingString: balanceString];
    
    return sectionName;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    //if(!cell){
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    //}
    NSDictionary* dict = [array objectAtIndex:indexPath.row];
    UILabel* cellDate = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 90, 40)];
    UILabel* cellTitle = [[UILabel alloc]initWithFrame:CGRectMake(cellDate.frame.origin.x + cellDate.frame.size.width + 10, 0, 200, 40)];
    UILabel* cellAmount = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width - 50, 0, 100, 40)];
    NSString* amountString = [NSString stringWithFormat:@"$%@", dict [@"Amount"]];
    cellTitle.text = dict [@"Title"];
    cellAmount.text = amountString;
    cellDate.text = dict [@"Date"];
    [cell addSubview:cellAmount];
    [cell addSubview:cellDate];
    [cell addSubview:cellTitle];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    viewExistingViewController* vevc = [viewExistingViewController new];
    vevc.transactionInformation = [array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vevc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

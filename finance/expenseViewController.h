//
//  expenseViewController.h
//  finance
//
//  Created by Mark Farrell on 2/4/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "viewExistingViewController.h"

@interface expenseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    UITableView* myTableView;
    NSMutableArray* array;
}

@end

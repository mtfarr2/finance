//
//  IncomeTableViewCell.h
//  finance
//
//  Created by Mark Farrell on 2/11/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncomeTableViewCell : UITableViewCell

@property(nonatomic, strong) UILabel* cellTitle;

@property(nonatomic,strong) UILabel* cellDate;

@property(nonatomic,strong) UILabel* cellAmount;


@end

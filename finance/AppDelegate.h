//
//  AppDelegate.h
//  finance
//
//  Created by Mark Farrell on 2/4/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

{
    NSUserDefaults* defaults;
    NSMutableDictionary* data;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray* expenseData;

@property (strong, nonatomic) NSMutableArray* incomeData;

@property (nonatomic) CGFloat bal;

@end


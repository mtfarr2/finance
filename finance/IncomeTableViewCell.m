//
//  IncomeTableViewCell.m
//  finance
//
//  Created by Mark Farrell on 2/11/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "IncomeTableViewCell.h"

@implementation IncomeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        self.backgroundColor = [UIColor redColor];
        _cellTitle = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, 200, 40)];
        _cellTitle.backgroundColor = [UIColor blueColor];
        _cellDate = [[UILabel alloc]initWithFrame:CGRectMake(0, _cellTitle.frame.origin.y + _cellTitle.frame.size.height, 100, 20)];
        _cellAmount = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width - 100, 0,100, 40)];
//        [self addSubview:_cellAmount];
//        [self addSubview:_cellDate];
//        [self addSubview:_cellTitle];
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  viewExistingViewController.m
//  finance
//
//  Created by Mark Farrell on 2/16/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "viewExistingViewController.h"

@interface viewExistingViewController ()

@end

@implementation viewExistingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Compose mail won't prepopulate message
    //how to sustain image after tapping back
    
    // Do any additional setup after loading the view.
    UIBarButtonItem* addPicture = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(addPictureTouched:)];
    UIBarButtonItem* sendEmail = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendEmailTouched:)];
    NSArray *actionBtns = @[addPicture, sendEmail];
    self.navigationItem.rightBarButtonItems = actionBtns;
    
    UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 70, self.view.frame.size.width, 40)];
    titleLabel.text = [NSString stringWithFormat:@"%@", _transactionInformation[@"Title"]];
    [self.view addSubview:titleLabel];
    UILabel* amountLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height, self.view.frame.size.width, 40)];
    amountLabel.text = [NSString stringWithFormat:@"$%@", _transactionInformation[@"Amount"]];
    [self.view addSubview:amountLabel];
    UILabel* dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, amountLabel.frame.origin.y + amountLabel.frame.size.height, self.view.frame.size.width, 40)];
    dateLabel.text = [NSString stringWithFormat:@"%@", _transactionInformation[@"Date"]];
    [self.view addSubview:dateLabel];
    
    iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, dateLabel.frame.origin.y + dateLabel.frame.size.height + 10,self.view.frame.size.width , 400)];
    iv.backgroundColor = [UIColor blackColor];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    iv.image = [self getMyImage:savedPath];
    [self.view addSubview:iv];
    
    
}

-(void)addPictureTouched:(id)sender
{
    popUpPictureOptions = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 200)];
    [popUpPictureOptions.layer setBorderWidth:2];
    [popUpPictureOptions.layer setCornerRadius:5];
    popUpPictureOptions.backgroundColor = [UIColor darkGrayColor];
    [self.view addSubview:popUpPictureOptions];
    
    UIButton* useExistingPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    useExistingPhoto.frame = CGRectMake(self.view.frame.size.width/2 - 75, 40, 150, 40);
    [useExistingPhoto setTitle:@"Use Existing" forState:UIControlStateNormal];
    [useExistingPhoto addTarget:self action:@selector(useExistingTouched:) forControlEvents:UIControlEventTouchUpInside];
    useExistingPhoto.backgroundColor = [UIColor purpleColor];
    [popUpPictureOptions addSubview:useExistingPhoto];
    
    UIButton* takePhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    takePhoto.frame = CGRectMake(self.view.frame.size.width/2 - 75, 120, 150, 40);
    [takePhoto setTitle:@"Take Photo" forState:UIControlStateNormal];
    [takePhoto addTarget:self action:@selector(takePhotoTouched:) forControlEvents:UIControlEventTouchUpInside];
    takePhoto.backgroundColor = [UIColor purpleColor];
    [popUpPictureOptions addSubview:takePhoto];
}

-(void)sendEmailTouched:(id)sender
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc]init];
    controller.mailComposeDelegate = self;
    NSString* messageBody = [NSString stringWithFormat:@"%@\n%@\n%@", _transactionInformation[@"Title"],_transactionInformation[@"Date"], _transactionInformation[@"Amount"] ];
    [controller setMessageBody:messageBody isHTML:NO];
    if (controller) [self presentViewController:controller animated:YES completion:^{
        
    }];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(void)useExistingTouched:(UIButton*)useExistingPhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)takePhotoTouched:(UIButton*)takePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    //    self.imageView.image = chosenImage;
    //    iv.image = chosenImage;
    
    NSDate* currentTime = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    
    [dateFormater setDateFormat:@"yyyy-MM-DD HH:mm:ss"];
    NSString *convertedDateString = [dateFormater stringFromDate:currentTime];
    
    
    
    savedPath = [self saveMyImage:chosenImage withTimeString:convertedDateString];
    
    iv.image = [self getMyImage:savedPath];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        
    }];
    [popUpPictureOptions removeFromSuperview];
}

-(UIImage*)getMyImage:(NSString*)pathString {
    
    return [UIImage imageWithContentsOfFile:pathString];
}

-(NSString*)saveMyImage:(UIImage*)myImage withTimeString:(NSString*)timeString {
    NSData *imageData = UIImagePNGRepresentation(myImage);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",timeString]];
    
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        return nil;
    }
    else
    {
        return imagePath;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

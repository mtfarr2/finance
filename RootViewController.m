//
//  RootViewController.m
//  finance
//
//  Created by Mark Farrell on 2/12/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    incomeViewController* ivc = [[incomeViewController alloc]init];
    
    expenseViewController* evc = [[expenseViewController alloc]init];
    
    TabbedViewController* tvc = [[TabbedViewController alloc]init];
    
    NSArray* vcsArray = @[
                          ivc,
                          evc
                          ];
    
    [tvc setViewControllers:vcsArray];
    
    [self.view addSubview:tvc.view];
    [self addChildViewController:tvc];
    
    UIBarButtonItem* addNew = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewTouched:)];
    NSArray *actionBtns = @[addNew];
    self.navigationItem.rightBarButtonItems = actionBtns;
    
}

-(void)addNewTouched:(id)sender{
    addNewViewController* anvc = [addNewViewController new];
    [self.navigationController pushViewController:anvc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

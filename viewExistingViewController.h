//
//  viewExistingViewController.h
//  finance
//
//  Created by Mark Farrell on 2/16/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface viewExistingViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate>
{
    UIView* popUpPictureOptions;
    UIImageView* iv;
    NSString* savedPath;
}

@property (nonatomic, strong) NSMutableDictionary* transactionInformation;


@end

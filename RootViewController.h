//
//  RootViewController.h
//  finance
//
//  Created by Mark Farrell on 2/12/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabbedViewController.h"
#import "incomeViewController.h"
#import "expenseViewController.h"
#import "addNewViewController.h"

@interface RootViewController : UIViewController

@end
